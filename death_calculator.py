labels = {
    'deaths': 'Total deaths:',
    'life_exp': 'Average life span (US):',
    'death_age': 'Average COVID death age:',
    'years_lost': 'Total years lost:',
    'adj_years': 'Adjusted years lost:',
    'adj_deaths': 'Adjusted death total:'
}

print('DEATH TO AMERICANS!\n  - The calculator')

while True:
    a = int(input(labels['deaths'] + ' '))
    b = float(input(labels['life_exp'] + ' '))
    c = float(input(labels['death_age'] + ' '))

    yr_exp = a * b
    yr_act = a * c
    print(labels['years_lost'], yr_act)

    adj_years = yr_exp - yr_act
    print(labels['adj_years'], adj_years)

    adj_deaths = adj_years / b
    print(labels['adj_deaths'], adj_deaths)

    input('\nPress [ENTER] to exit...')

    break